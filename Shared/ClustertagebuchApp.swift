//
//  ClustertagebuchApp.swift
//  Shared
//
//  Created by Michael Helmbrecht on 09.10.20.
//

import SwiftUI




@main
struct ClustertagebuchApp: App {
  @AppStorage(wrappedValue: false, "onboardingCompleted") var onboardingCompleted: Bool
  @StateObject var authService = AuthenticationService.shared
  @Environment(\.scenePhase) var scenePhase
  @Environment(\.openURL) var openURL
  @StateObject var store = AppStore(initialState: AppState(),
                                    reducer: appReducer,
                                    environment: AppEnvironment())
  
  var body: some Scene {
    WindowGroup {
      
//      ZStack {
      MainAppView()
        .environmentObject(store)

        //
        //        BiometricView {
        //          authService.authenticate()
        //        }
        //        .hidden(authService.isAuthenticated() || !onboardingCompleted)
        
//        OnboardingContainerView()
//          .hidden(onboardingCompleted)
//      }
    }
    
    .onChange(of: scenePhase) { (scenePhase) in
      switch scenePhase{
      case .active:
        print("scene active")
        store.send(.load)
      //        if authService.shouldTryAuthentication() && !authService.isAuthenticating {
      //          authService.authenticate()
      //        }
      
      //        if isAuthenticationWithBiometrics {
      //          if !authService.isUnlocked
      //              && authService.authError == nil
      //              && isAuthenticationWithBiometrics {
      //            authService.authenticate()
      //          }
      //
      //          if authService.isUnlocked {
      //            store.send(.load)
      //          }
      //        } else {
      //          store.send(.load)
      //        }
      case .inactive:
        store.send(.save)
        //        if !authService.isAuthenticating {
        //          authService.lock()
        //        }
        print("scene inactive")
      case .background:
        print("scene in Background")
      @unknown default:
        break
      }
    }
  }
}
