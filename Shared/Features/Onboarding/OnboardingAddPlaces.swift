//
//  OnboardingAddPlaces.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 17.10.20.
//

import SwiftUI

struct OnboardingAddPlaces: View {
  @State var store: AppStore
  @State private var text: String = ""
  var places: [Place]
  let nextButtonPressed: () -> Void
  var body: some View {
    ScrollView {
      VStack(alignment: .leading) {
        Text("Orte hinzufügen").font(.title).bold()
          .padding(.top, 32)
        TextField("", text: $text) { (changed) in
          print("changed")
        } onCommit: {
          //TODO: Implement
          text = ""
        }.textFieldStyle(RoundedBorderTextFieldStyle())
        
        LazyVGrid(columns: [GridItem(.flexible(minimum: 40, maximum: 375)), GridItem(.flexible(minimum: 40, maximum: 375))], content: {
          ForEach(places, id: \.uri) { (person) in
            Pill(title: person.name, color: Color.color2).font(.body)
          }
        })
        Spacer()
        HStack {
          Spacer()
          StandardButton(title: "weiter") {
            nextButtonPressed()
          }
          
          Spacer()
        }.padding()
        
          
      }.padding()
    }
  }
}

struct OnboardingAddPlaces_Previews: PreviewProvider {
    static var previews: some View {
      OnboardingAddPlaces(store: AppStore.mockStore(), places: []) {
        
      }
    }
}
