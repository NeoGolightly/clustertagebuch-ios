//
//  OnboardingMain.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 16.10.20.
//

import SwiftUI

struct OnboardingContainerView: View {
  @EnvironmentObject var store: AppStore
  @AppStorage("onboardingCompleted") var onboardingCompleted: Bool?
  @State private var selected = 0
  @State private var allPeople: [Person] = []
  @State private var allPlaces: [Place] = []
  var body: some View {
    TabView(selection: $selected) {
      OnboardingStart(nextButtonPressed: nextButtonPressed).tag(0)
      OnboardingAddPeople(store: store, people: allPeople, nextButtonPressed: nextButtonPressed).tag(1)
      OnboardingAddPlaces(store: store, places: allPlaces, nextButtonPressed: nextButtonPressed).tag(2)
      OnboardingEnd(doneButtonPressed: doneButtonPressed).tag(3)
    }
    .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
  }
  
  private func doneButtonPressed() {
    withAnimation {
      onboardingCompleted = true
    }
  }
  
  private func nextButtonPressed() {
    if selected < 3 {
      withAnimation {
        selected += 1
      }
    }
  }
  
}

struct OnboardingMain_Previews: PreviewProvider {
  static var previews: some View {
    OnboardingContainerView()
  }
}
