//
//  OnboardingEnd.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 16.10.20.
//

import SwiftUI

struct OnboardingEnd: View {
  
  
  let doneButtonPressed: () -> Void
  var body: some View {
    ScrollView {
      VStack(alignment: .leading) {
        Text("Super! Der größte Teil ist geschafft 🥳").font(.largeTitle).bold().padding()
        Text("Etiam porta sem malesuada magna mollis euismod. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum id ligula porta felis euismod semper. \n\nEtiam porta sem malesuada magna mollis euismod. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum id ligula porta felis euismod semper. ").font(.title3).padding()
        
        
        
      }.padding()
      HStack {
        Spacer()
        StandardButton(title: "Fertig") {
          doneButtonPressed()
        }
        
        Spacer()
      }.padding()
    }
  }
}

struct OnboardingEnd_Previews: PreviewProvider {
  static var previews: some View {
    OnboardingEnd {
      
    }
  }
}
