//
//  OnboardingAddPeople.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 16.10.20.
//

import SwiftUI

struct OnboardingAddPeople: View {
  @State var store: AppStore
  var people: [Person]
  @State private var isEditingText: Bool = false
  @State private var text: String = ""
  let nextButtonPressed: () -> Void
  var body: some View {
    ScrollView {
      VStack(alignment: .leading) {
        Text("Personen hinzufügen").font(.title).bold()
          .padding(.top, 32)
        TextField("", text: $text) { (changed) in
          print("changed")
          isEditingText = true
        } onCommit: {
          let person = Person(name: text)
          store.send(.updatePerson(person))
          text = ""
        }.textFieldStyle(RoundedBorderTextFieldStyle())
        
        LazyVGrid(columns: [GridItem(.flexible(minimum: 40, maximum: 375)), GridItem(.flexible(minimum: 40, maximum: 375))], content: {
          ForEach(people, id: \.uri) { (person) in
            Pill(title: person.name).font(.body)
          }
        })
        
        
        Spacer()
        if (!isEditingText) {
          HStack {
            Spacer()
            StandardButton(title: "weiter") {
              nextButtonPressed()
            }
            
            Spacer()
          }.padding()
        }
        
      }.padding()
    }
  }
}

struct OnboardingAddPeople_Previews: PreviewProvider {
  static var previews: some View {
    OnboardingAddPeople(store: AppStore.mockStore(),
                        people: Person.mocks()) {
      
    }
  }
}
