//
//  OnboardingStart.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 16.10.20.
//

import SwiftUI

struct OnboardingStart: View {
  let nextButtonPressed: () -> Void
  let font1 = Font.system(.body, design: .default).leading(.loose)
  var body: some View {
    ScrollView {
      VStack(alignment: .leading) {
        Text("Willkommen im Clustertagebuch")
          .font(Font.largeTitle.bold())
          .foregroundColor(Color(red: 186/255, green: 86/255, blue: 137/255))
          .padding()
        Text("123124124 Etiam porta sem malesuada magna mollis euismod. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum id ligula porta felis euismod semper. \n\nEtiam porta sem malesuada magna mollis euismod. Maecenas faucibus mollis interdum. Aenean lacinia bibendum nulla sed consectetur. Nullam id dolor id nibh ultricies vehicula ut id elit. Vestibulum id ligula porta felis euismod semper. ").font(font1).padding()
        
        
        
      }.padding()
      HStack {
        Spacer()
        StandardButton(title: "weiter") {
          nextButtonPressed()
        }
        
        Spacer()
      }.padding()
    }
  }
}

struct OnboardingStart_Previews: PreviewProvider {
  static var previews: some View {
    OnboardingStart {
      
    }
    
  }
}
