//
//  DiaryContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 04.11.20.
//

import SwiftUI

struct DiaryContainerView: View {
  
  @StateObject var store: AppStore
  
  private var clusteredContacts: [DiaryDayRepresentation] {
    store.state
      .allContacts
      .clusteredContacts(contacts: store.state.contacts)
      .map({DiaryDayRepresentation(date: $0.key,
                                   people: $0.value.peopleCount(),
                                   places: $0.value.placesCount())})
      .sorted(by: {$0.date < $1.date})
  }
  
  var body: some View {
    DiaryView(daysWithContacts: clusteredContacts)
      .navigationTitle("Tagebuch")
      .embedInNavigation()
  }
}

struct DiaryContainerView_Previews: PreviewProvider {
    static var previews: some View {
      DiaryContainerView(store: AppStore.mockStore())
    }
}
