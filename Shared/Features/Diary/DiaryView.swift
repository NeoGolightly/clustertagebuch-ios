//
//  DiaryView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 04.11.20.
//

import SwiftUI
import SwiftDate

struct DiaryDayRepresentation: Hashable {
  let date: Date
  let people: Int
  let places: Int
}

extension DiaryDayRepresentation {
  static func mocks() -> [DiaryDayRepresentation] {
    Contact.mocks().map({DiaryDayRepresentation(date: $0.key,
                                                people: $0.value.peopleCount(),
                                                places: $0.value.placesCount())})
  }
}

struct DiaryView: View {
  var daysWithContacts: [DiaryDayRepresentation]
  var body: some View {
    VStack(spacing: 0) {
      DiaryHeadView()
        .padding(.horizontal)
        .padding(.top, 0)
        .padding(.bottom)
      Rectangle().frame(height: 1).foregroundColor(.gray)
      ScrollView{
        Color.clear.frame(width: 20, height: 16)
        HStack {
          Text("01.10.20 - 08.10.20").padding(.horizontal)
          Spacer()
        }
        ForEach(daysWithContacts, id:\.self) { item in
          Button(action: {}, label: {
            DiaryViewCell(day: item)
              .padding(.horizontal)
          }).accentColor(.primary)
        }
      }
    }
  }
}

struct DiaryView_Previews: PreviewProvider {
  static var previews: some View {
    DiaryView(daysWithContacts: DiaryDayRepresentation.mocks().sorted(by: {$0.date < $1.date}))
  }
}
struct DiaryHeadView: View {
  
  private func numberFormat(number: Int) -> String {
    return number < 10 ? "0\(number)" : "\(number)"
  }
  
  var body: some View {
    VStack(alignment: .leading) {
      HStack() {
        Text("01.10 - 14.10.20")
        Spacer()
      }
      HStack(alignment: .firstTextBaseline) {
        VStack(alignment: .leading) {
          Text("Deine letzten")
            .bold()
            .lineLimit(1)
            .fixedSize(horizontal: true, vertical: false)
          Text("14 Tage").bold().lineLimit(1)
        }.font(.title)
        Spacer(minLength: 24)
        VStack(alignment: .leading) {
          HStack {
            Text("Personen")
            Spacer()
            Text("\(numberFormat(number:[1,2,3,4,5,6,7,8,9,10, 28].randomElement()!))")
              .font(Font.system(.title2, design: .monospaced))
              .bold()
              .foregroundColor(.color1)
          }
          HStack {
            Text("Orte")
              
            Spacer()
            Text("\(numberFormat(number:[1,2,32,4,5,64,7,8,9,10, 28].randomElement()!))")
              .font(Font.system(.title2, design: .monospaced))
              .bold()
              .foregroundColor(.color2)
          }.font(Font.system(.body, design: .monospaced))
          
        }
      }
      
    }
  }
}


struct DiaryViewCell: View {
  
  var day: DiaryDayRepresentation
  
  private func numberFormat(number: Int) -> String {
    return number < 10 ? "0\(number)" : "\(number)"
  }
  
  private func format(date: Date) -> String {
    let formater = DateFormatter()
    formater.dateFormat = "dd.MM.yy"
    return formater.string(from: date)
  }
  
  var body: some View {
    HStack {
      
      VStack(alignment: .leading) {
        HStack {
          Text(day.date.weekdayName(SymbolFormatStyle.default))
            .font(.title3)
            .bold()
            .foregroundColor(.gray)
          Spacer()
        }
        Text(day.date.toFormat("dd.MM.yy"))
          .font(.title2)
          .bold()
          .foregroundColor(.color3)
        Spacer()
      }
//      Spacer(minLength: UIScreen.main.bounds.width * 0.05)
      Divider()
//      Spacer(minLength: UIScreen.main.bounds.width * 0.05)
      VStack(alignment: .leading) {
        HStack {
          Text("Personen")
          Spacer()
          Text(numberFormat(number: day.people))
            .font(Font.system(.title3, design: .monospaced))
            .bold()
            .foregroundColor(.color1)
        }
        HStack {
          Text("Orte")
          Spacer()
          Text("\(numberFormat(number: day.places))")
            .font(Font.system(.title3, design: .monospaced))
            .bold()
            .foregroundColor(.color2)
        }
        
      }.padding(.leading).font(Font.system(.body, design: .monospaced))
      Spacer()
    }
    .padding()
    .background(RoundedRectangle(cornerRadius: 16, style: .continuous)
                  .fill(Color.white)
                  .buttonShadow())
  }
}

struct DiaryViewCell_Previews: PreviewProvider {
  static var previews: some View {
    DiaryViewCell(day: DiaryDayRepresentation.mocks()[0])
      .previewLayout(.sizeThatFits)
  }
}
