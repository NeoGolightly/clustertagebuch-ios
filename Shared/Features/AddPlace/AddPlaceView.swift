//
//  AddPlaceView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI

struct AddPlaceView: View {
  @Binding var name: String
  var save: (() -> Void)?
  var cancel: (() -> Void)?
  var delete: (() -> Void)?
  var body: some View {
    Form {
      Section(header: Text("Bezeichnung")
                .font(.sectionTitleFont)
                .foregroundColor(.primary),
              footer: Text("*erforderlich")) {
        TextField("Bezeichnung*", text: $name)
          .disableAutocorrection(true)
      }.textCase(nil)
      Section(header: Text("Adresse")
                .font(.sectionTitleFont)
                .foregroundColor(.primary)) {
        TextField("Straße + Hausnummer", text: .constant(""))
        TextField("Stadt", text: .constant(""))
        TextField("Telefonnummer", text: .constant(""))
        TextField("Email", text: .constant(""))
      }.textCase(nil)
    }
    .navigationTitle("Ort hinzugügen")
    .toolbar(content: {
      //Save
      ToolbarItem(placement: .confirmationAction) {
        Button(action: {
          (save ?? {})()
        }, label: {
          Text("Speichern")
        })
      }
      //Dismiss
      ToolbarItem(placement: .cancellationAction) {
        Button(action: {
          (cancel ?? {})()
        }, label: {
          Text("Abbrechen")
        })
      }
    })
  }
}

struct AddPlaceView_Previews: PreviewProvider {
  static var previews: some View {
    AddPlaceView(name: .constant("")).embedInNavigation()
  }
}
