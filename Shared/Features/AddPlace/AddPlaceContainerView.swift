//
//  AddPlaceContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI

struct AddPlaceContainerView: View {
  @Environment(\.presentationMode) private var presentationMode
  @State private var showDeleteActionSheet = false
  @State private var name = ""
  @State var place: Place?
  @State var store: AppStore
  
  private func save(){
    var uri: String
    //use exiting uri or create a new one
    if let place = place { uri = place.uri }
    else {uri = UUID().uuidString}
    let newPlace = Place(uri: uri, name: name)
    store.send(.updatePlace(newPlace))
    store.send(.save)
    dismiss()
  }
  
  private func delete() {
    if let place = place {
      store.send(.removePlace(place))
      store.send(.save)
      dismiss()
    }
  }
  
  private func deletePressed() {
    showDeleteActionSheet.toggle()
  }
  
  private func dismiss() {
    presentationMode.wrappedValue.dismiss()
  }
  
  var body: some View {
    AddPlaceView(name: $name,
                 save: save,
                 cancel: dismiss,
                 delete: delete)
      .onAppear(perform: {
        if let place = place {
          name = place.name
        }
      })
      .actionSheet(isPresented: $showDeleteActionSheet) {
        ActionSheet(title: Text("\(place?.name ?? "Ort") löschen?"),
                    message: Text("Diese Aktion kann nicht rückgängig gemacht werden!"), buttons: [
            .destructive(Text("Löschen"), action: {delete()}),
            .cancel()
          ])
      }
  }
}

struct AddPlaceContainerView_Previews: PreviewProvider {
    static var previews: some View {
      AddPlaceContainerView(place: nil,
                            store: AppStore.mockStore())
    }
}
