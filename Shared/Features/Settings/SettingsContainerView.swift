//
//  SettingsContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 20.10.20.
//

import SwiftUI

struct SettingsContainerView: View {
  
  var body: some View {
    SettingsView()
      .embedInNavigation()
  }
}

struct SettingsContainerView_Previews: PreviewProvider {
  static var previews: some View {
    SettingsContainerView()
  }
}
