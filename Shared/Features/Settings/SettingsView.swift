//
//  SettingsView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 20.10.20.
//

import SwiftUI
import LocalAuthentication

struct SettingsView: View {
  @Environment(\.presentationMode) var presentationMode
  @State var authService = AuthenticationService.shared
  @AppStorage(wrappedValue: false, "onboardingCompleted") var onboardingCompleted: Bool
  
  var body: some View {
    Form {
      if AuthenticationService.isTouchIDPossible() {
        Section(header: Text("Sicherheit")) {
          Toggle("TouchID benutzen", isOn: $authService.shouldAuthenticateWithBiometrics)
        }
      }
      
      if AuthenticationService.isFaceIDPossible() {
        Section(header: Text("Sicherheit")) {
          Toggle("FaceID benutzen", isOn: $authService.shouldAuthenticateWithBiometrics)
        }
      }
      
      Section {
        Button("Onboarding erneut starten") {
          onboardingCompleted = false
          presentationMode.wrappedValue.dismiss()
        }
      }
      
    }
    .navigationTitle("Einstellunge")
    .toolbar(content: {
      ToolbarItem(placement: .primaryAction ) {
        Button("Fertig") {
          presentationMode.wrappedValue.dismiss()
        }
      }
    })
  }
}

struct SettingsView_Previews: PreviewProvider {
  static var previews: some View {
    SettingsView()
  }
}

