//
//  TrackContactViewV2.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 28.10.20.
//

import SwiftUI

struct TrackContactView: View {
  
  enum TimePickerType {
    case duration
    case time
  }
  
  @Binding var name: String
  @Binding var date: Date
  @Binding var duration: Duration
  @Binding var startTime: Date
  @Binding var endTime: Date
  @Binding var people: [Person]
  @Binding var place: Place?
  @Binding var useStartEndTime: Bool
  @Binding var noMinimumDistance: Bool
  @Binding var noMask: Bool
  var selectPeople: (() -> Void)?
  var selectPlace: (() -> Void)?
  var save: (() -> Void)?
  
  @State private var criticalContact = false
  @State private var timePickerType: TimePickerType = .duration

  var body: some View {
    Form {
      Section(header: Text("Was?").font(.sectionTitleFont).foregroundColor(.primary)) {
        TextField("Beschreibung", text: $name).disableAutocorrection(true)
      }.textCase(nil)
      //
      //Wann?
      //
      Section(header: Text("Wann?").font(.sectionTitleFont).foregroundColor(.primary)) {
        DatePicker("Datum", selection: $date, displayedComponents: .date)
          .datePickerStyle(DefaultDatePickerStyle())
        
        HStack() {
          Text("Dauer")
          Picker("Dauer", selection: $duration) {
            ForEach(Duration.allCases, id: \.self) { (duration) in
              Text(duration.rawValue).tag(duration.rawValue)
            }
          }.pickerStyle(SegmentedPickerStyle())
        }
        Toggle("Start- / End-Zeit Auswählen", isOn: $useStartEndTime)
        if useStartEndTime {
          DatePicker("Start", selection: .constant(Date()), displayedComponents: .hourAndMinute)
            .datePickerStyle(DefaultDatePickerStyle())
          DatePicker("Ende", selection: .constant(Date()), displayedComponents: .hourAndMinute)
            .datePickerStyle(DefaultDatePickerStyle())
        }
      }.textCase(nil)
      //
      //Wer?
      //
      PillTrackingSection<Person>(title: "Wer?",
                                  addButtonTitle: people.isEmpty ? "Person auswählen" : "Auswahl ändern",
                                  data: people,
                                  color: .color1) {
        (selectPeople ?? {})()
      }
      //
      //Wo?
      //
      PillTrackingSection<Place>(title: "Wo?",
                                 addButtonTitle:"Ort auswählen",
                                 data: (place != nil) ? [place!] : [],
                                 color: .color2) {
        (selectPlace ?? {})()
      }
      Section {
        
        Toggle("Kritischer Kontakt", isOn: $criticalContact)
          .toggleStyle(SwitchToggleStyle(tint: .red))
          .foregroundColor(.red)
        if criticalContact {
          Toggle("Kein Mindestabstand", isOn: $noMinimumDistance)
          Toggle("Keine Maske", isOn: $noMask)
        }
      }
      
      Section {
        HStack {
          Spacer()
          Button("Speichern") {
            (save ?? {})()
          }
          Spacer()
        }
      }
    }

    
  }
}

//struct TrackContactView_Previews: PreviewProvider {
//  static var previews: some View {
//    TrackContactView(name: .constant(""),
//                     date: .constant(Date()),
//                     place: .constant([Place.mocks()[0]]))
//      .preferredColorScheme(.dark)
//      .embedInNavigation()
//  }
//}



struct PillTrackingSection<Data: Nameable&URIable>: View {
  var title: String
  var addButtonTitle: String
  var data: [Data]
  var color: Color
  var action: () -> Void
  
  var body: some View {
    Section(header: Text(title).font(.sectionTitleFont).foregroundColor(.primary)) {
      if !data.isEmpty {
        ScrollView(.horizontal, showsIndicators: false, content: {
          HStack(alignment: .center, spacing: 8){
            Color.clear.frame(width: 8, height: 1)
            ForEach(data, id: \.uri) { (value) in
              ZStack{
                Text(value.name)
                  .font(Font.caption.leading(.tight).bold())
                  .lineLimit(2)
                  .frame(minWidth: 30, idealWidth: 80, minHeight: 30)
                  .padding([.top, .bottom], 8)
                  .padding([.leading, .trailing], 16)
                Capsule().stroke(color).foregroundColor(.clear)
                
              }
            }.padding([.top, .bottom], 1)
            .padding([.leading, .trailing], 1)
            Color.clear.frame(width: 8, height: 1)
          }
        })
        .listRowInsets(EdgeInsets(top: 8, leading: 0, bottom: 8, trailing: 0))
      }
      
      Button(action: {
        action()
      }, label: {
        Label(addButtonTitle, systemImage: data.isEmpty ? "checkmark.circle" : "checkmark.circle.fill")
      })
      
    }.accentColor(color).textCase(nil)
  }
}
