//
//  TrackContactContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 03.11.20.
//

import SwiftUI

struct TrackContactContainerView: View {
  
  class TrackContactContainerState: ObservableObject {
    @Published var sheetType: TrackContactContainerView.SheetType = .none
  }
  
  enum SheetType {
    case none, selectPeople, selectPlace
  }
  
  @Environment(\.presentationMode) var presentationMode
  @State var store: AppStore
  var preselectedPerson: Person?
  var preselectedPlace: Place?
  @State var name: String = ""
  @State var date: Date = Date()
  @State var duration: Duration = .quater
  @State var startTime: Date = Date()
  @State var endTime: Date = Date()
  @State var people: [Person] = []
  @State var place: Place?
  @State var useStartEndTime = false
  @State var noMinimumDistance: Bool = false
  @State var noMask: Bool = false
  @State private var showSheet = false
  @State private var showAlert = false
  @StateObject private var state = TrackContactContainerState()
  
  private var allPeople: [Person] {
    store.state.people.compactMap {
      store.state.allPeople[$0]
    }.sorted(by: {$0.name > $1.name})
  }
  
  private func save() {
    if place == nil && people.isEmpty {
      showAlert.toggle()
    }
    let newContact = Contact(name: name,
                             date: date,
                             duration: duration,
                             startTime: startTime,
                             endTime: endTime,
                             people: people.compactMap{$0.uri},
                             place: place?.uri,
                             useStartEndTime: useStartEndTime,
                             noMinimumDistance: noMinimumDistance,
                             noMask: noMask)
    store.send(.updateContact(newContact))
    store.send(.save)
    dismiss()
  }
  
  private func dismiss() {
    presentationMode.wrappedValue.dismiss()
  }
  
  private func showSelectPeopleSheet() {
    state.sheetType = .selectPeople
    showSheet.toggle()
  }
  
  private func showSelectPlaceSheet() {
    state.sheetType = .selectPlace
    showSheet.toggle()
  }

  
  var body: some View {
      TrackContactView(name: $name,
                       date: $date,
                       duration: $duration,
                       startTime: $startTime,
                       endTime: $endTime,
                       people: $people,
                       place: $place,
                       useStartEndTime: $useStartEndTime,
                       noMinimumDistance: $noMinimumDistance,
                       noMask: $noMask,
                       selectPeople: showSelectPeopleSheet,
                       selectPlace: showSelectPlaceSheet,
                       save: save
      )
      
      .onAppear(perform: {
        if let person = preselectedPerson{ people.append(person) }
        place = preselectedPlace
      }).sheet(isPresented: $showSheet,
               onDismiss: {
                
               },
               content: {
                switch (state.sheetType) {
                case .selectPeople:
                  SelectPersonContainerView(store: store,
                                            selectedPeople: $people)
                case .selectPlace:
                  SelectPlaceContainerView(store: store,
                                           selectedPlace: $place)
                  EmptyView()
                case .none:
                  EmptyView()
                }
               })
      .alert(isPresented: $showAlert, content: {
        Alert(title: Text("Auswahl nicht vollständig").foregroundColor(.red),
              message: Text("ganz toller text hier, der erklrt, warum die Auswahl nicht vollständig ist"),
              dismissButton: .default(Text("na gut 🙄")))
      })
      .navigationTitle("Kontakt tracken").toolbar(content: {
        ToolbarItem(placement: .cancellationAction) {
          Button("Abbrechen") {
            dismiss()
          }
        }
        
        ToolbarItem(placement: .confirmationAction) {
          Button("Speichern") {
            save()
          }
        }
        
      })
  }
}

struct TrackContactContainerView_Previews: PreviewProvider {
  static var previews: some View {
    TrackContactContainerView(store: AppStore.mockStore())
  }
}
