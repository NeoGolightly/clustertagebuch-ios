//
//  DirectoryContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI



extension Binding where Value == Dictionary<String, Person>{
  func bindDictionar() -> Binding<[Value.Value]>{
    Binding<[Value.Value]> { () -> [Value.Value] in
      Array(self.wrappedValue.values)
    } set: { (item) in
      
    }

  }
}


class DirectoryContainerState: ObservableObject {
  @Published var showSheet = false
  @Published var sheetType: DirectoryContainerView.SheetType = .none
}

struct DirectoryContainerView: View {
  
  enum SheetType {
    case none
    case addPerson
    case addPlace
    case addTemplate
  }
  @EnvironmentObject var store: AppStore
  @StateObject var viewState = DirectoryContainerState()
  @State private var selectedPerson: Person?
  @State private var selectedPlace: Place?
  @State private var allPeople: [Person] = []
  @State private var allPlaces: [Place] = []
  @State private var allTemplates: [Template] = []
  
  private var people: [Person] {
    store.state.people.compactMap {
      store.state.allPeople[$0]
    }
  }
  
  private func showAddPersonView(person: Person?) {
    selectedPerson = person
    viewState.sheetType = .addPerson
    viewState.showSheet.toggle()
  }
  
  private func showAddPlaceView(place: Place?) {
    selectedPlace = place
    viewState.sheetType = .addPlace
    viewState.showSheet.toggle()
  }
  
  private func showAddTemplateView() {
    viewState.sheetType = .addTemplate
    viewState.showSheet.toggle()
  }
  private func updateValues() {
    allPeople = store.state.allPeople.valuesToArray()
    allPlaces = Array(store.state.allPlaces.values)
  }
  
  var body: some View {
    DirectoryView(people: people,
                  places: $allPlaces,
                  templates: store.binding(for: \.allTemplates),
                  addPerson: showAddPersonView,
                  addPlace: showAddPlaceView,
                  addTemplate: showAddTemplateView)
      .embedInNavigation()
      .onChange(of: store.state, perform: { state in
        updateValues()
      })
      .onAppear(perform: {
        updateValues()
      })
      .sheet(isPresented: $viewState.showSheet, onDismiss: {
        
      }, content: {
        switch viewState.sheetType {
        case .addPerson:
          AddPersonContainerView(person: selectedPerson,
                                 store: store)
            .embedInNavigation()
        case .addPlace:
          AddPlaceContainerView(place: selectedPlace,
                                store: store)
            .embedInNavigation()
        case .addTemplate:
          AddTemplateContainerView().embedInNavigation()
        default:
          EmptyView()
        }
      })
  }
}

struct DirectoryContainerView_Previews: PreviewProvider {
  static var previews: some View {
    DirectoryContainerView()
  }
}
