//
//  DirectoryView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI

struct DirectoryView: View {
  
  enum DirectoryPickerType: String, CaseIterable {
    case people = "Personen"
    case places = "Orte"
    case templates = "Vorlagen"
  }
  
  @State private var pickerType: DirectoryPickerType = .people
  var people: [Person]
  @Binding var places: [Place]
  @Binding var templates: [Template]
  var addPerson: (Person?) -> Void
  var addPlace: (Place?) -> Void
  var addTemplate: () -> Void
  
  var body: some View {
    VStack {
      Picker("Entry Type", selection: $pickerType) {
        ForEach(DirectoryPickerType.allCases, id: \.self) { (pickerType) in
          Text(pickerType.rawValue).tag(pickerType.rawValue)
        }
      }
      .pickerStyle(SegmentedPickerStyle())
      .padding([.leading, .trailing])
      TabView(selection: $pickerType,
              content:  {
                //People
                VStack {
                  PillGridView<Person>(data: people, pillColor: .color1) { (person) in
                    addPerson(person)
                  }
                  Spacer()
                  DirectoryAddButton(label: "Person hinzufügen", systemImage: Icon.person.rawValue) {
                    addPerson(nil)
                  }.accentColor(.color1)
                }.tag(DirectoryPickerType.people)
                //Places
                VStack {
                  PillGridView<Place>(data: places, pillColor: .color2) { (place) in
                    addPlace(place)
                  }
                  Spacer()
                  DirectoryAddButton(label: "Ort hinzufügen", systemImage: Icon.place.rawValue) {
                    addPlace(nil)
                  }.accentColor(.color2)
                }.tag(DirectoryPickerType.places)
                //Templates
                VStack {
                  
                  ScrollView {
                    LazyVGrid(columns: [GridItem(.flexible())], alignment: .center, spacing: 0) {
                      ForEach(templates, id: \.uri) { (template) in
                        TemplatePillView(template: .constant(template)).padding(.horizontal).padding(.top)
                      }
                    }
                  }
                  Spacer()
                  DirectoryAddButton(label: "Template hinzufügen", systemImage: Icon.template.rawValue) {
                    addTemplate()
                  }
                }.tag(DirectoryPickerType.templates)
              })
        .tabViewStyle(PageTabViewStyle(indexDisplayMode: .never))
    }.navigationTitle("Verzeichnis")
  }
  
  
}

struct DirectoryView_Previews: PreviewProvider {
  static var previews: some View {
    Group {
      TabView {
        NavigationView {
          DirectoryView(people: Person.mocks(),
                        places: .constant(Place.mocks()),
                        templates: .constant(Template.mocks()),
                        addPerson: {_ in},
                        addPlace: {_ in},
                        addTemplate: {})
        }
      }
      
    }
  }
}

struct PillGridView<Data: DataIdentifiable>: View {
  private let columns = [
    GridItem(.adaptive(minimum: 100))
  ]
  
  var data: [Data]
  var pillColor: Color
  var dataSelected: (Data?) -> Void
  
  var body: some View {
    ScrollView {
      LazyVGrid(columns: columns, alignment: .center, spacing: nil, content: {
        ForEach(data, id: \.uri) { (object) in
          Button(action: {
            dataSelected(object)
          }, label: {
            Pill(title: object.name, color: pillColor)
          }).foregroundColor(.primary)
        }
      }).padding()
    }
  }
}

struct DirectoryAddButton: View {
  var label: String
  var systemImage: String
  var action: () -> Void
  
  @Environment(\.colorScheme) var colorScheme
  
  var body: some View {
    Button(action: {
      action()
    }, label: {
      HStack(alignment: .firstTextBaseline) {
        Label(label, systemImage: systemImage)
      }.padding().frame(minWidth: 0, maxWidth: .infinity).background(colorScheme == .light ? Color.white.cornerRadius(16).buttonShadow() :
                                                                      Color.white.opacity(0.1).cornerRadius(16).buttonShadow())
    }).padding([.leading, .trailing, .bottom])
  }
}
