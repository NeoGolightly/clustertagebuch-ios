//
//  SelectDataContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 03.11.20.
//

import SwiftUI

struct SelectPersonContainerView: View {
  @Environment(\.presentationMode) var presentationMode
  @State var store: AppStore
  @Binding var selectedPeople: [Person]
  private var people: [Person] {
    store.state.people
      .compactMap{ store.state.allPeople[$0] }
      .sorted(by: {$0.name > $1.name})
  }
  
  private func dismiss() {
    presentationMode.wrappedValue.dismiss()
  }
  
  var body: some View {
    SelectDataView<Person>(data: people,
                           selectedData: $selectedPeople,
                           color: .color1,
                           select: dismiss)
      .navigationTitle("Person(en)")
      .toolbar(content: {
        ToolbarItem(placement: .confirmationAction) {
          Button(action: {
            presentationMode.wrappedValue.dismiss()
          }, label: {
            Text("Auswählen")
          }).disabled(selectedPeople.isEmpty)
        }
        
        ToolbarItem(placement: .cancellationAction) {
          Button(action: {
            presentationMode.wrappedValue.dismiss()
          }, label: {
            Text("Abbrechen")
          })
        }
      })
      .embedInNavigation()
  }
}

//struct SelectDataContainerView_Previews: PreviewProvider {
//    static var previews: some View {
//        SelectDataContainerView()
//    }
//}
