//
//  SelectPlaceContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 03.11.20.
//

import SwiftUI

struct SelectPlaceContainerView: View {
  @Environment(\.presentationMode) var presentationMode
  @State var store: AppStore
  @Binding var selectedPlace: Place?
  @State var tempSelectedPlaces: [Place] = []
  private var places: [Place] {
    store.state.places
      .compactMap{ store.state.allPlaces[$0] }
      .sorted(by: {$0.name < $1.name})
  }
  
  private func dismiss() {
    presentationMode.wrappedValue.dismiss()
  }
  
  var body: some View {
    SelectDataView<Place>(data: places,
                          selectedData: $tempSelectedPlaces,
                          color: .color2,
                          singleSelection: true,
                          select: dismiss)
      .onAppear(perform: {
        tempSelectedPlaces = selectedPlace == nil ? [] : [selectedPlace!]
      })
      .onChange(of: tempSelectedPlaces, perform: { value in
        selectedPlace = tempSelectedPlaces.first
      })
      .navigationTitle("Ort")
      .toolbar(content: {
        ToolbarItem(placement: .confirmationAction) {
          Button(action: {
            presentationMode.wrappedValue.dismiss()
          }, label: {
            Text("Auswählen")
          }).disabled(tempSelectedPlaces.isEmpty)
        }
        
        ToolbarItem(placement: .cancellationAction) {
          Button(action: {
            presentationMode.wrappedValue.dismiss()
          }, label: {
            Text("Abbrechen")
          })
        }
      })
      .embedInNavigation()
  }
}

//struct SelectPlaceContainerView_Previews: PreviewProvider {
//    static var previews: some View {
//        SelectPlaceContainerView()
//    }
//}
