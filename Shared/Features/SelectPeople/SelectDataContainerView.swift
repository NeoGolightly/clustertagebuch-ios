//
//  SelectDataContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 03.11.20.
//

import SwiftUI

struct SelectPersonContainerView<Data>: View where Data: DataIdentifiable{
  
  @State var store: AppStore
  @Binding var selectedData: [Data]
  var body: some View {
    SelectDataView<Data>(data: <#T##[_]#>, selectedData: <#T##Binding<[_]>#>)
  }
}

//struct SelectDataContainerView_Previews: PreviewProvider {
//    static var previews: some View {
//        SelectDataContainerView()
//    }
//}
