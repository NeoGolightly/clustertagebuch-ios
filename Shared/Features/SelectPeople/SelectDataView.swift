//
//  SelectDataView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 03.11.20.
//

import SwiftUI

struct SelectDataView<Data>: View  where Data: DataIdentifiable{
  private let columns = [
    GridItem(.adaptive(minimum: 100))
  ]
  
  var data: [Data]
  @Binding var selectedData: [Data]
  var color: Color
  var singleSelection = false
  var select: (() -> Void)?
  
  
  var body: some View {
    VStack {
      ScrollView {
        LazyVGrid(columns: columns, alignment: .center, spacing: nil, content: {
          ForEach(data, id: \.uri) { (item) in
            Button(action: {
              if selectedData.contains(where: {$0.uri == item.uri}) {
                selectedData.removeAll(where: { $0.uri == item.uri})
              }else{
                if singleSelection { selectedData.removeAll() }
                selectedData.append(item)
                
              }
            }, label: {
              Pill(title: item.name,
                   color: color,
                   selected: selectedData.contains(where: {$0.uri == item.uri}))
            }).foregroundColor(.primary)
          }
        }).padding()
        
        
      }
      Spacer()
      Button(action: {
        (select ?? {})()
      },
      label: {
        Text("Auswählen")
          .padding()
          .padding([.leading, .trailing], 16)
          .foregroundColor(.white)
          .background(Color.accentColor.cornerRadius(8))
      }).disabled(selectedData.isEmpty)
    }
  }
}

struct SelectDataView_Previews: PreviewProvider {
    static var previews: some View {
      SelectDataView<Person>(data: [],
                             selectedData: .constant([]),
                             color: .color1)
    }
}
