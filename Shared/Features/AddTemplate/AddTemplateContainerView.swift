//
//  AddTemplateContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI

struct AddTemplateContainerView: View {
    var body: some View {
        AddTemplateView()
    }
}

struct AddTemplateContainerView_Previews: PreviewProvider {
    static var previews: some View {
        AddTemplateContainerView()
    }
}
