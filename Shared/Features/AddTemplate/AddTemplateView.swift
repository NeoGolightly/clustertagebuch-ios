//
//  AddTemplateView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//


import SwiftUI

struct AddTemplateView: View {
  
  @State var people: [Person] = []
  @State var place: Place?
  @State private var repeating = false
  @State private var sendNotification = false
  @State private var repeatingSelection: Int = 0
  @State private var daySelection: Int = -1
  
  var body: some View {
    Form {
      Section(header: Text("Titel")
                .font(.sectionTitleFont)
                .foregroundColor(.primary),
              footer: Text("*erforderlich")) {
        TextField("Titel*", text: .constant(""))
      }.textCase(nil)
      addPeopleSection()
      addPlaceSection()
      
      
      Section(header:Text("Wann?")
                .font(.sectionTitleFont)
                .foregroundColor(.primary)) {
        Toggle("Regelmäßig", isOn: $repeating)
        if repeating {
          dateSelection()
          
          Picker("Wiederholung", selection: $repeatingSelection) {
            ForEach(RepeatingType.allCases, id: \.self) { (repeatingType) in
              Text(repeatingType.rawValue)
                .tag(RepeatingType.allCases.firstIndex(of: repeatingType) ?? -1)
            }
          }
          
          if repeatingSelection == RepeatingType.allCases.firstIndex(of: .everyWeek) ||
              repeatingSelection == RepeatingType.allCases.firstIndex(of: .everyTwoWeeks) {
            Picker("Tag", selection: $daySelection) {
              ForEach(Day.allCases, id: \.self) { (day) in
                Text(day.rawValue)
                  .tag(Day.allCases.firstIndex(of: day) ?? -1)
              }
            }
          }
          
          Toggle("Erinnerung senden", isOn: $sendNotification)
        }
      }
    }
    .navigationTitle("Template hinzugügen")
    .toolbar(content: {
      ToolbarItem(placement: .confirmationAction) {
        Button(action: {
          
        }, label: {
          Text("Speichern")
        })
      }
      
      ToolbarItem(placement: .cancellationAction) {
        Button(action: {
          
        }, label: {
          Text("Abbrechen")
        })
      }
    })
  }
  
  private func addPeopleSection() -> some View {
    Section(header: Text("Wer?")
              .font(.sectionTitleFont)
              .foregroundColor(.primary)) {
      if !people.isEmpty {
        ScrollView(.horizontal, showsIndicators: false, content: {
          HStack(alignment: .center, spacing: 8){
            ForEach(people, id: \.uri) { (person) in
              ZStack{
                Text(person.name)
                  .font(Font.caption.leading(.tight).bold())
                  .lineLimit(2)
                  .frame(minWidth: 30, idealWidth: 80, minHeight: 30)
                  .padding([.top, .bottom], 8)
                  .padding([.leading, .trailing], 16)
                Capsule().stroke(Color.color1).foregroundColor(.clear)
                
              }
            }.padding([.top, .bottom], 1)
            .padding([.leading, .trailing], 1)
          }
        })
      }
      
      Button(action: {}, label: {
        Label("Personen hinzufügen", systemImage: "plus.circle.fill")
        //.labelStyle(IconOnlyLabelStyle())
      })
      
    }.accentColor(Color.color1).textCase(nil)
  }
  
  private func addPlaceSection() -> some View {
    Section(header: Text("Wo?")
              .font(.sectionTitleFont)
              .foregroundColor(.primary)) {
      if place != nil {
        ScrollView(.horizontal, showsIndicators: false, content: {
          HStack(alignment: .center, spacing: 8){
            ZStack{
              Text(place?.name ?? "")
                .font(Font.caption.leading(.tight).bold())
                .lineLimit(2)
                .frame(minWidth: 30, idealWidth: 80, minHeight: 30)
                .padding([.top, .bottom], 8)
                .padding([.leading, .trailing], 16)
              Capsule().stroke(Color.color2).foregroundColor(.clear)
              
            }
            .padding([.top, .bottom], 1)
            .padding([.leading, .trailing], 1)
          }
        })
      }
      
      Button(action: {}, label: {
        Label("Ort hinzufügen", systemImage: "plus.circle.fill")
        //.labelStyle(IconOnlyLabelStyle())
      })
      
    }.accentColor(Color.color2).textCase(nil)
  }
  private func dateSelection() -> some View {
    
    Group {
      DatePicker("Start", selection: .constant(Date()), displayedComponents: .hourAndMinute)
        .datePickerStyle(DefaultDatePickerStyle())
      DatePicker("Ende", selection: .constant(Date()), displayedComponents: .hourAndMinute)
        .datePickerStyle(DefaultDatePickerStyle())
    }
  }
}

struct AddTemplateView_Previews: PreviewProvider {
  static var previews: some View {
    AddTemplateView().embedInNavigation()
  }
}
