//
//  HomeContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 17.10.20.
//
import SwiftUI
import SwiftDate



struct HomeContainerView: View {
  
  class HomeContainerState: ObservableObject {
    @Published var sheetType: HomeContainerView.SheetType = .none
  }
  
  enum SheetType {
    case none, settings, track
  }
  
  @StateObject var store: AppStore
  @StateObject var state = HomeContainerState()
  @State private var showSheet = false
  @State private var selectedPerson: Person?
  @State private var selectedPlace: Place?
  
  private var people: [Person] {
    store.state.people.compactMap {
      store.state.allPeople[$0]
    }.sorted(by: {$0.name < $1.name})
  }
  
  private var places: [Place] {
    store.state.places.compactMap {
      store.state.allPlaces[$0]
    }
  }
  
  private var clusteredContacts: [Date : [Contact]] {
    store.state.allContacts.clusteredContacts(contacts: store.state.contacts)
  }
  
  func addPerson(person: Person?) {
    state.sheetType = .track
    selectedPerson = person
    showSheet.toggle()
  }
  
  func addPlace(place: Place?) {
    state.sheetType = .track
    selectedPlace = place
    showSheet.toggle()
  }
  
  func showSettings() {
    state.sheetType = .settings
    showSheet.toggle()
  }
  
  func resetSelectedPersonAndPlace() {
    selectedPlace = nil
    selectedPerson = nil
  }
  
  var body: some View {
    
    TrackingView(people: people,
                 places: places,
                 clusteredContacts: clusteredContacts,
                 trackPerson: addPerson,
                 trackPlace: addPlace,
                 showSettings: showSettings
    )
    .embedInNavigation()
    .tag("home")
    .navigationViewStyle(StackNavigationViewStyle())
    .sheet(isPresented: $showSheet,
           onDismiss: {
            resetSelectedPersonAndPlace()
           },
           content: {
            switch (state.sheetType) {
            case .settings:
              SettingsContainerView()
            case .track:
              TrackContactContainerView(store: store,
                                        preselectedPerson: selectedPerson,
                                        preselectedPlace: selectedPlace)
                .embedInNavigation()
            case .none:
              EmptyView()
            }
           })
    .onOpenURL(perform: { url in
      if url.absoluteString == "clustertagebuch2020/widgetTracking"{
        print("link aus widget")
        addPerson(person: nil)
      }
    })
  }
}


struct HomeContainerView_Previews: PreviewProvider {
  static var previews: some View {
    TabView{
      HomeContainerView(store: AppStore.mockStore())
    }
  }
}
