//
//  Home.swift
//  Clustertagebuch
//
//  Created by Michael Helmbrecht on 09.10.20.
//

import SwiftUI

struct TrackingView: View {
  var people: [Person]
  var places: [Place]
  var clusteredContacts: [Date: [Contact]]
  
  var trackPerson: ((Person?) -> Void)?
  var trackPlace: ((Place?) -> Void)?
  var showSettings: (() -> Void)?
  
  var body: some View {
    VStack(alignment: .leading, spacing: 0) {
      if UIDevice.current.type != .iPhoneSE {
        TrackingHeaderScrollView(clusteredContacts: clusteredContacts)
          .padding()
      }
      
      Text("Meine Vorlagen").font(Font.title.bold())
          .padding(.horizontal)
      ScrollView(.horizontal, showsIndicators: false) {
        LazyHGrid(rows: [GridItem(.fixed(30))], alignment: .center) {
          Color.clear.frame(width: 10, height: 0)
          ForEach(Template.mocks() + Template.mocks(), id: \.uri) { (template) in
            TinyTemplatePillView(template: .constant(template)).frame(minWidth: 64, maxWidth: 160)
          }
          Color.clear.frame(width: 10, height: 0)
        }.frame(maxHeight: 44)
        .padding(.vertical, 4)
        .padding(.top, 8)
      }.padding(.bottom)
      
  
        
      Text("Tracken").font(Font.title.bold())
        .padding(.horizontal)
        .padding(.bottom)
  
      HStack(alignment: .top, spacing: 16) {
        TrackingShortcutView<Person>(data: people,
                                     selected: trackPerson,
                                     color: Color.color1,
                                     label: "Person",
                                     systemName: Icon.personFill.rawValue)
        TrackingShortcutView<Place>(data: places,
                                    selected: trackPlace,
                                    color: Color.color2,
                                    label: "Ort",
                                    systemName: Icon.placeFill.rawValue)
      }.padding([.leading, .trailing])
      
    }
    .navigationBarTitle("Kontakte tracken", displayMode: .inline)
    .toolbar(content: {
      ToolbarItem(placement: .navigationBarTrailing) {
        Menu {
          Button("Person") {
            (trackPerson ?? {_ in})(nil)
          }
          Button("Ort") {
            (trackPlace ?? {_ in})(nil)
          }
          Divider()
          Button("Vorlage") {
            
          }
        } label: {
          Button(action: {}, label: {
            Label("Tracken", systemImage: "plus.circle.fill").font(.title2)
          })
        }
      }
      ToolbarItem(placement: .navigationBarLeading, content: {
        Button(action: {
          (showSettings ?? {})()
        }, label: {
          Label("Settings", systemImage: "gear")
        })
      })
    })
  }
}

struct Home_Previews: PreviewProvider {
  static var previews: some View {
    
    TrackingView(people: Person.mocks(),
                 places: Place.mocks(),
                 clusteredContacts: [:])
      .embedInNavigation()
//      .preferredColorScheme(.dark)
  }
}

struct TrackingShortcutView<Tracker: Nameable&URIable>: View {
  @Environment(\.colorScheme) var colorScheme
  
  var data: [Tracker]
  var selected: ((Tracker?) -> Void)?
  var color: Color
  var label: String
  var systemName: String
  
  var body: some View {
    VStack {
      //
      //Tracking List Button
      //
      Button(action: {
        (selected ?? {_ in})(nil)
      }, label: {
        HStack(alignment: .firstTextBaseline) {
          Text(label).foregroundColor(.white).font(.title3).bold()
          Spacer()
            Image(systemName: systemName).font(.title2)
              .foregroundColor(.white)
        }
      }).buttonStyle(TrackenButtonStyle(tintColor: color))
      //
      //Tacking Data List
      //
      ScrollView(showsIndicators: false) {
        Rectangle().frame(minHeight: 8).foregroundColor(.clear)
        ForEach(data, id: \.uri) { data in
          Button(action: {
            (selected ?? {_ in})(data)
          }, label: {
            
            Pill(title: data.name, color: color)
              .padding([.leading, .trailing], 8)
          }).foregroundColor(.primary)
        }
        Rectangle().frame(minHeight: 8).foregroundColor(.clear)
      }.padding(.top, -8)
    }
  }
}

struct HomeHeaderView: View {
  @Environment(\.colorScheme) var colorScheme
  @Binding var dates: [String]
  
  var body: some View {
    ScrollView(.horizontal, showsIndicators: false) {
      ScrollViewReader(content: { proxy in
        
        HStack(spacing: 16) {
          
          //2
          Color.clear.frame(width: 0, height: 0)
          ForEach(dates, id: \.self) { item in
            Button(action: {
              
            }, label: {
              
              VStack(alignment: .leading) {
                Text("Heute").font(.caption).padding(.top, 8)
                Text(item).font(.title).bold().padding(.bottom, -4)
                HStack {
                  Rectangle().frame(height: 1)
                  Spacer(minLength: 80)
                }
                HStack {
                  Text("17").foregroundColor(.color1).font(Font.system(size: 20, weight: .bold, design: .monospaced))
                  Text ("Personen").font(Font.system(size: 20, weight: .bold, design: .monospaced))
                }
                HStack {
                  Text("04").foregroundColor(.color2).font(Font.system(size: 20, weight: .bold, design: .monospaced))
                  Text ("Orte").font(Font.system(size: 20, weight: .bold, design: .monospaced))
                  
                }
              }
              .padding()
              .background(colorScheme == .light ? Color.white.cornerRadius(20).cardShadow() :
                            Color.white.opacity(0.1).cornerRadius(20).cardShadow())
            }).accentColor(.primary)
            .padding([.top, .bottom], 16)
            .frame(idealWidth: 330)
            .id(item)
            .disabled(true)
          }
          Color.clear.frame(width: 0, height: 0)
        }.padding(.bottom, 16)
        
      })
    }
  }
}
