//
//  TrackingHeaderView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 31.10.20.
//

import SwiftUI

struct TrackingHeaderScrollView: View {
  var clusteredContacts: [Date : [Contact]]
  
  private func peopleCount(date: Date) -> Int {
    return clusteredContacts[date]?.peopleCount() ?? 0
  }
  
  private func placesCount(date: Date) -> Int {
    return clusteredContacts[date]?.placesCount() ?? 0
  }
  
  
  
  var body: some View {
    ScrollView(.horizontal, showsIndicators: false) {
      ScrollViewReader { scrollProxy in
        HStack(spacing: 16) {
          if clusteredContacts.keys.count == 0 {
//            Color.clear.frame(width: UIScreen.main.bounds.width * 0.1, height: 2)
            TrackingHeaderCardView(date: Date(), peopleContactsCount: 0, placesVisitedCount: 0)
              .frame(width: UIScreen.main.bounds.width * 0.8)
          } else {
            ForEach(clusteredContacts.keysToArray().sorted(by: <), id: \.self) { (date) in
              TrackingHeaderCardView(date: date,
                                     peopleContactsCount: peopleCount(date: date),
                                     placesVisitedCount: placesCount(date: date))
                .frame(width: UIScreen.main.bounds.width * 0.8)
                .id(date)
            }
          }
        }
        .padding(35)
        .onChange(of: clusteredContacts, perform: { value in
          scrollProxy.scrollTo(clusteredContacts.keysToArray().sorted(by: <).last, anchor: .center)
        })
        .onAppear(perform: {
          scrollProxy.scrollTo(clusteredContacts.keysToArray().sorted(by: <).last, anchor: .center)
        })
      }
    }
    .padding(-35)
  }
}

struct TrackingHeaderView_Previews: PreviewProvider {
  static var previews: some View {
    Group {
      TrackingHeaderScrollView(clusteredContacts: Contact.mocks())
      TrackingHeaderCardView(date: Date(),
                             peopleContactsCount: 14,
                             placesVisitedCount: 7)
        .previewLayout(.sizeThatFits)
    }
  }
}



struct TrackingHeaderCardView: View {
  @Environment(\.colorScheme) var colorScheme
  var date: Date
  var peopleContactsCount: Int
  var placesVisitedCount: Int
  
  var body: some View {
      VStack(alignment: .leading, spacing: 0){
        HStack {
          if date.isToday {Text("Heute").font(.caption)}
          if date.isYesterday {Text("Gestern").font(.caption)}
         
          Spacer()
          Image(systemName: "info.circle.fill")
            .foregroundColor(.gray)
        }
        //Date
        Text(format(date: date))
          .font(.title)
          .bold()
          .lineLimit(1)
        HStack {
          Rectangle().frame(height: 1)
          Spacer(minLength: 40)
        }.padding(.bottom)
        HStack {
          Text(numberFormat(number: peopleContactsCount))
            .font(Font.system(size: 20, weight: .bold, design: .monospaced))
            .foregroundColor(.color1)
          Text ("Personen").font(Font.system(size: 20, weight: .bold, design: .monospaced))
        }
        HStack {
          Text(numberFormat(number: placesVisitedCount))
            .font(Font.system(size: 20, weight: .bold, design: .monospaced))
            .foregroundColor(.color2)
          Text ("Orte").font(Font.system(size: 20, weight: .bold, design: .monospaced))
          
        }
      }
      .padding()
      .background(RoundedRectangle(cornerRadius: 20, style: /*@START_MENU_TOKEN@*/.continuous/*@END_MENU_TOKEN@*/)
                    .fill(colorScheme == .light ? Color.white : Color.white.opacity(0.1))
                    .cardShadow()
      )
   
  }
  
  private func numberFormat(number: Int) -> String {
    return number < 10 ? "0\(number)" : "\(number)"
  }
  
  private func format(date: Date) -> String {
    let formater = DateFormatter()
    formater.dateFormat = "dd.MM.yy"
    return formater.string(from: date)
  }
}
