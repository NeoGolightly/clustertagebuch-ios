//
//  AddPersonView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI

struct AddPersonView: View {
  @Binding var name: String
  @Binding var telephone: String
  @Binding var email: String
  var save: (() -> Void)?
  var cancel: (() -> Void)?
  var delete: (() -> Void)?
  
  var body: some View {
    Form {
      //Name Section
      Section(header: Text("Name")
                .font(.sectionTitleFont).bold()
                .foregroundColor(.primary),
              footer: Text("*erforderlich")) {
        TextField("Name*", text: $name)
          .disableAutocorrection(true)
      }.textCase(nil)
      //Telephone and Email Section
      Section(header: Text("Kontaktdaten")
                .font(.sectionTitleFont).bold()
                .foregroundColor(.primary)) {
        TextField("Telefonnummer", text: $telephone)
          .disableAutocorrection(true)
          .keyboardType(.phonePad)
        TextField("Email", text: $email)
          .disableAutocorrection(true)
          .keyboardType(.emailAddress)
      }.textCase(nil)
      if let delete = delete {
        //TODO: – Make Button Style
        //(no pressed style in Form when button is full size)
        Section {
          Button(action: {
            delete()
          }, label: {
            Text("Löschen")
              .font(Font.body.bold())
              .foregroundColor(.white)
              .frame(maxWidth: .infinity, maxHeight: .infinity)
              .background(Color.red)
          })
        }
        .listRowInsets(EdgeInsets())
      }
    }
    .navigationTitle("Person hinzugügen")
    .toolbar(content: {
      ToolbarItem(placement: .confirmationAction) {
        Button(action: {
          (save ?? {})()
        }, label: {
          Text("Speichern")
        })
      }
      
      ToolbarItem(placement: .cancellationAction) {
        Button(action: {
          (cancel ?? {})()
        }, label: {
          Text("Abbrechen")
        })
      }
    })
  }
}

struct AddPersonView_Previews: PreviewProvider {
  static var previews: some View {
    AddPersonView(name: .constant("Daniel Düsentrieb"),
                  telephone: .constant("05234342344"),
                  email: .constant("daniel.d@disney.com"))
      .embedInNavigation()
  }
}
