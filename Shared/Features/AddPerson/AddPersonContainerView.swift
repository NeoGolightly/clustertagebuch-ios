//
//  AddPersonContainerView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI

struct AddPersonContainerView: View {
  @Environment(\.presentationMode) private var presentationMode
  @State private var showDeleteActionSheet = false
  @State private var name = ""
  @State private var telephone = ""
  @State private var email = ""
  @State var person: Person?
  @State var store: AppStore
  
  private func save(){
    var uri: String
    //use exiting uri or create a new one
    if let person = person { uri = person.uri }
    else {uri = UUID().uuidString}
    let newPerson = Person(uri: uri, name: name, telephone: telephone, email: email)
    store.send(.updatePerson(newPerson))
    store.send(.save)
    dismiss()
  }
  
  private func delete() {
    if let person = person {
      store.send(.removePerson(person))
      store.send(.save)
      dismiss()
    }
  }
  
  private func deletePressed() {
    showDeleteActionSheet.toggle()
  }
  
  private func dismiss() {
    presentationMode.wrappedValue.dismiss()
  }
  
  var body: some View {
    AddPersonView(name: $name,
                  telephone: $telephone,
                  email: $email,
                  save: save,
                  cancel: dismiss,
                  delete: (person != nil) ? deletePressed : nil )
      .onAppear(perform: {
        if let person = person {
          name = person.name
          telephone = person.telephone
          email = person.email
        }
      })
      .actionSheet(isPresented: $showDeleteActionSheet) {
        ActionSheet(title: Text("\(person?.name ?? "Person") löschen?"),
                    message: Text("Diese Aktion kann nicht rückgängig gemacht werden!"), buttons: [
            .destructive(Text("Löschen"), action: {delete()}),
            .cancel()
          ])
      }
  }
}

struct AddPersonContainerView_Previews: PreviewProvider {
  static var previews: some View {
    AddPersonContainerView(person: nil, store: AppStore.mockStore())
  }
}
