//
//  Icons.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 29.10.20.
//

import Foundation

enum Icon: String {
  case person = "person.2"
  case personFill = "person.2.fill"
  case place = "signpost.right"
  case placeFill = "signpost.right.fill"
  case template = "doc.plaintext"
  case templateFill = "doc.plaintext.fill"
  case tracken = "pencil"
  case diary = "book"
  case directory = "list.bullet"
  case statistics = "chart.bar.xaxis"
}
