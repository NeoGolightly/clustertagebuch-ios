//
//  ButtonStyles.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 29.10.20.
//

import SwiftUI

struct ViewM {
  @SceneStorage("fsadf") var data: Bool = false
}

struct TrackenButtonStyle: ButtonStyle {
  var tintColor: Color = .primary
  func makeBody(configuration: Configuration) -> some View {
    configuration
      .label
      
      .padding()
      .frame(maxHeight: 64)
      .background(
        Group {
          if configuration.isPressed {
            RoundedRectangle(cornerRadius: 16, style: .continuous)
              .fill(tintColor/*.opacity(configuration.isPressed ? 0.2 : 1)*/)
          }
          else {
            RoundedRectangle(cornerRadius: 16, style: .continuous)
              .fill(tintColor)
              .buttonShadow()
          }
        }
      )
      .opacity(configuration.isPressed ? 0.2 : 1)
      
  }
}
