//
//  Fonts.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI


extension Font {
  static let sectionTitleFont = Font.title2.bold()
}
