//
//  BiometricView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 25.10.20.
//

import SwiftUI

struct BiometricView: View {
  var action: () -> Void
  var body: some View {
    ZStack{
      Color.white
      VStack {
        Image(systemName: AuthenticationService.isFaceIDPossible()
                ? "faceid" : "touchid").font(.largeTitle )
        Button("Erneut ensperren") {
          action()
        }
      }
      
    }
  }
}

struct BiometricView_Previews: PreviewProvider {
    static var previews: some View {
      BiometricView(action: {})
    }
}
