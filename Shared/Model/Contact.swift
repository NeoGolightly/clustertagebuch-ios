//
//  Contact.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 17.10.20.
//

import Foundation
import SwiftDate

struct Contact: Codable, Hashable, DataIdentifiable {
  let uri: String
  let name: String
  let date: Date
  let duration: Duration?
  let startTime: Date?
  let endTime: Date?
  let people: [String]
  let place: String?
  let useStartEndTime: Bool
  let noMinimumDistance: Bool
  let noMask: Bool
  
  init(uri: String = UUID().uuidString,
       name: String,
       date: Date,
       duration: Duration? = nil,
       startTime: Date? = nil,
       endTime: Date? = nil,
       people: [String] = [],
       place: String? = nil,
       useStartEndTime: Bool = false,
       noMinimumDistance: Bool = false,
       noMask: Bool = false) {
    self.uri = uri
    self.name = name
    self.date = date.dateTruncated(from: .hour)!
    self.duration = duration
    self.startTime = startTime
    self.endTime = endTime
    self.people = people
    self.place = place
    self.useStartEndTime = useStartEndTime
    self.noMinimumDistance = noMinimumDistance
    self.noMask = noMask
  }
}

extension Contact {
  static func mocks() -> [Date : [Contact]] {
    let contacts = [
      Contact(name: "Sport", date: Date(), people: Person.mocks().map{$0.uri}, place: Place.mocks()[0].uri),
      Contact(name: "Kaffeetrinken", date: Calendar.current.date(byAdding: .day, value: -1, to: Date())!),
      Contact(name: "Arbeit", date: Calendar.current.date(byAdding: .day, value: -1, to: Date())!),
      Contact(name: "Sport", date: Calendar.current.date(byAdding: .day, value: -2, to: Date())!)
    ]
    return contacts.reduce([Date : [Contact]]()) { (result, contact) -> [Date : [Contact]] in
      var dic = result
      if let date = contact.date.dateTruncated(from: .hour) {
        print("debug \(date)")
        dic[date] = dic[date] ?? [] + [contact]
      }
      return dic
    }
  }
}


