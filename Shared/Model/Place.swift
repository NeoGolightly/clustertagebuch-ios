//
//  Place.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 17.10.20.
//

import Foundation

struct Place: Codable, Hashable, DataIdentifiable {
  let uri: String
  let name: String
  
  init(uri: String = UUID().uuidString, name: String) {
    self.uri = uri
    self.name = name
  }
}

extension Place {
  static func mocks() -> [Place] {
    return [
      Place(name: "Aprilkind"),
      Place(name: "Wildfang"),
      Place(name: "Tanzschule")
    ]
  }
}
