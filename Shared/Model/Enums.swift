//
//  Enums.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import Foundation

enum RepeatingType: String, CaseIterable, Codable {
  case irregular = "unregelmäßig"
  case everyDay = "jeden Tag"
  case everyWeekDay = "jeden Wochentag"
  case everyWorkDay = "jeden Werktag"
  case everyWeek = "jede Woche"
  case everyTwoWeeks = "jede 2. Woche"
  case everyMonth = "jeden Monat"
}

enum Duration: String, CaseIterable, Codable {
  case quater = "15"
  case halfAnHour = "30"
  case oneHour = "60"
  case twoHours = "120"
  case overTwoHours = ">120"
}

enum Day: String, CaseIterable, Codable {
  case monday = "Montag"
  case tuesday = "Dienstag"
  case wednesday = "Mittwoch"
  case thursday = "Donnerstag"
  case friday = "Freitag"
  case saturday = "Samstag"
  case sunday = "Sonntag"
}
