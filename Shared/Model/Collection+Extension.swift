//
//  Collection+Extension.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 03.11.20.
//

import Foundation
import SwiftDate

extension Dictionary {
  func valuesToArray() -> [Value] {
    Array(self.values)
  }
  
  func keysToArray() -> [Key] {
    Array(self.keys)
  }
}

extension Dictionary where Key == String, Value == Contact {
  func clusteredContacts(contacts: [String]) -> [Date : [Contact]]{
    let value = contacts.compactMap{self[$0]}
      .reduce([Date : [Contact]]()) { (result, contact) -> [Date : [Contact]] in
        var dic = result
        if let date = contact.date.dateTruncated(from: .hour), var array = dic[date] ?? [] {
          array.append(contact)
          dic[date] = array
        }
        return dic
      }
    return value
  }
}

extension Sequence where Iterator.Element == Contact {
  func peopleCount() -> Int {
    return self
      .map({$0.people})
      .reduce([], +)
      .removingDuplicates()
      .count
  }
  
  func placesCount() -> Int {
    return self
      .compactMap({$0.place})
      .removingDuplicates()
      .count
  }
}
