//
//  App.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 17.10.20.
//

import Foundation
import Combine
import Codextended
import WidgetKit

typealias AppStore = Store<AppState, AppAction>

struct AppEnvironment {
  let decoder = JSONDecoder()
  let encoder = JSONEncoder()
  let files = FileManager.default
  let fileName = "state.json"
}

struct AppState: Codable, Equatable {
  var allContacts: [String : Contact] = [:]
  var contacts: [String] = []
  var allPeople: [String : Person] = [:]
  var people: [String] = []
  var allPlaces: [String : Place] = [:]
  var places: [String] = []
  var allTemplates: [Template] = []
  
  enum CodingKeys: CodingKey {
    case allContacts
    case contacts
    case allPeople
    case people
    case allPlaces
    case places
    case allTemplates
  }
  
  init(){}
  
  init(from decoder: Decoder) throws {
    allContacts = try decoder.decode(CodingKeys.allContacts)
    contacts = try decoder.decode(CodingKeys.contacts)
    allPeople = try decoder.decode(CodingKeys.allPeople)
    people = (try? decoder.decode(CodingKeys.people) as [String]) ?? []
    allPlaces = try decoder.decode(CodingKeys.allPlaces)
    places = (try? decoder.decode(CodingKeys.places) as [String]) ?? []
    allTemplates = try decoder.decode(CodingKeys.allTemplates) as [Template]
  }
}

enum AppAction {
  case updateContact(Contact)
  case removeContact(Contact)
  case updatePerson(Person)
  case removePerson(Person)
  case updatePlace(Place)
  case removePlace(Place)
  case set(state: AppState)
  case resetState
  case load
  case save
  case deleteAll
}

let appReducer: Reducer<AppState, AppAction, AppEnvironment> = Reducer { state, action, environment in
  switch action {
  //
  //Contact Stuff
  //
  case let .updateContact(contact):
    state.allContacts[contact.uri] = contact
    state.contacts = state.allContacts.map { $0.value.uri }
  case let .removeContact(contact):
    state.allContacts.removeValue(forKey: contact.uri)
    state.contacts.removeAll(where: {$0 == contact.uri})
  //
  //People Stuff
  //
  case let .updatePerson(person):
    state.allPeople[person.uri] = person
    state.people = state.allPeople.map{ $0.value.uri}
  case let .removePerson(person):
    state.allPeople.removeValue(forKey: person.uri)
    state.people.removeAll(where: {$0 == person.uri})
  //
  //Places Stuff
  //
  case let .updatePlace(place):
    state.allPlaces[place.uri] = place
    state.places = state.allPlaces.map{ $0.value.uri}
  case let .removePlace(place):
    state.allPlaces.removeValue(forKey: place.uri)
    state.places.removeAll(where: {$0 == place.uri})
  case .resetState:
    return Empty().eraseToAnyPublisher()
  case let .set(newState):
    state = newState
    state.people = state.allPeople.map{ $0.value.uri }
    state.places = state.allPlaces.map{ $0.value.uri }
    state.contacts = state.allContacts.map { $0.value.uri }
  case .load:
    return environment.files
      .readStore(name: environment.fileName)
      .decode(type: AppState.self, decoder: environment.decoder)
      .print()
      .replaceError(with: AppState())
      .map { AppAction.set(state: $0) }
      .eraseToAnyPublisher()
  case .save:
    WidgetCenter.shared.reloadAllTimelines()
    return Just(state)
      .encode(encoder: environment.encoder)
      .flatMap { environment.files.writeStore(data: $0, name: environment.fileName) }
      .print()
      .map { _ in AppAction.resetState }
      .replaceError(with: .resetState)
      .eraseToAnyPublisher()
  case .deleteAll:
    state = AppState()

  }
  return Empty(completeImmediately: true).eraseToAnyPublisher()
}

extension AppStore {
  static func mockStore() -> AppStore {
    AppStore(initialState: AppState(),
             reducer: appReducer,
             environment: AppEnvironment())
  }
}

