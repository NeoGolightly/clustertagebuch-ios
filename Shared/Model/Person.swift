//
//  Person.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 17.10.20.
//

import Foundation
import Codextended

struct Person: Codable, Hashable, DataIdentifiable{
  
  let uri: String
  let name: String
  let telephone: String
  let email: String
  init(uri: String = UUID().uuidString, name: String, telephone: String = "", email: String = "") {
    self.uri = uri
    self.name = name
    self.telephone = telephone
    self.email = email
  }
  
  enum CodingKeys: CodingKey {
    case uri
    case name
    case telephone
    case email
  }

  init(from decoder: Decoder) throws {
    uri = try decoder.decode(CodingKeys.uri)
    name = try decoder.decode(CodingKeys.name)
    telephone = try decoder.decode(CodingKeys.telephone)
    email = try decoder.decode(CodingKeys.email)
  }
  
}



extension Person {
  static func mocks() -> [Person] {
    return [
      Person(name: "Michael Helmbrecht"),
      Person(name: "Niclas Bauermeister"),
      Person(name: "Nikita Schirra"),
      Person(name: "Mark-Uwe Kling"),
      Person(name: "Benjamin von Stuckrad-Barre"),
      Person(name: "Christian Drosten"),
      Person(name: "Name")
    ]
  }
}
