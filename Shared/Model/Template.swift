//
//  Template.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import Foundation

struct Template: Codable, Hashable, DataIdentifiable  {
  let uri: String
  let name: String
  let people: [Person]
  let place: Place?
  let isRepeating: Bool
  let startTime: Date?
  let endTime: Date?
  let repeatingType: RepeatingType
  let repeatingDay: Day?
  let sendNotifications: Bool
  
  init(uri: String = UUID().uuidString,
       name: String,
       people: [Person] = [],
       place: Place? = nil,
       isRepeating: Bool = false,
       startTime: Date? = nil,
       endTime: Date? = nil,
       repeatingType: RepeatingType = .irregular,
       repeatingDay: Day? = nil,
       sendNotifications: Bool = false) {
    self.uri = uri
    self.name = name
    self.people = people
    self.place = place
    self.isRepeating = isRepeating
    self.startTime = startTime
    self.endTime = endTime
    self.repeatingType = repeatingType
    self.repeatingDay = repeatingDay
    self.sendNotifications = sendNotifications
  }
}

extension Template {
  static func mocks() -> [Template] {
    return [
      Template(name: "Tanzkreis",
               people: Person.mocks(),
               place: Place(name: "Tanzschule"),
               isRepeating: true,
               startTime: Calendar.current.date(bySettingHour: 17, minute: 00, second: 0, of: Date())!,
               endTime: Calendar.current.date(bySettingHour: 21, minute: 00, second: 0, of: Date())!,
               repeatingType: .everyWeek,
               repeatingDay: .tuesday,
               sendNotifications: true),
      Template(name: "Arbeit"),
      Template(name: "Serienabend")
    ]
  }
}


