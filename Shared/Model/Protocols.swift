//
//  Protocols.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 31.10.20.
//

import Foundation
protocol URIable {
  var uri: String { get }
}

protocol Nameable {
  var name: String { get }
}

typealias DataIdentifiable = URIable&Nameable
