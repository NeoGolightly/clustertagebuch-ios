//
//  AuthenticationService.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 27.10.20.
//

import LocalAuthentication
import SwiftUI
import Combine

class AuthenticationService: ObservableObject {
  @AppStorage(wrappedValue: false, "shouldAuthenticateWithBiometrics") var shouldAuthenticateWithBiometrics: Bool
  private let context = LAContext()
  var error: NSError?
  @Published private var isUnlocked: Bool = false
  @Published var authError: Error?
  @Published var isAuthenticating: Bool = false
  
  enum BiometricType{
    case touch
    case face
    case none
  }
  
  static let shared = AuthenticationService()
  
  private init() {
    
  }
  
  func shouldTryAuthentication() -> Bool {
    return shouldAuthenticateWithBiometrics && !isUnlocked
  }
  
  func isAuthenticated() -> Bool {
    return shouldAuthenticateWithBiometrics ? isUnlocked : true
  }
  
  func lock() {
    isUnlocked = false
  }
  
  func authenticate() {
    let context = LAContext()
    var error: NSError?
    
    // check whether biometric authentication is possible
    if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
//      context.localizedFallbackTitle = ""
      // it's possible, so go ahead and use it
      let reason = "We need to unlock your data."
      context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, authenticationError in
        // authentication has now completed
        
        DispatchQueue.main.async {
          self.isAuthenticating = true
        }
          if success {
            DispatchQueue.main.async {
              self.authError = nil
              self.isUnlocked = true
              self.isAuthenticating = false
            }
          } else {
            print("use user passcode")
            context.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: reason) { success, authenticationError in
              // authentication has now completed
              DispatchQueue.main.async {
                if success {
                  self.authError = nil
                  self.isUnlocked = true
                  self.isAuthenticating = false
                } else {
                  self.isUnlocked = false
                  self.authError = authenticationError
                  self.isAuthenticating = false
                  print("evaluatePolicy Error: \(authenticationError)")
                }
              }
            }
          }
        
      }
    } else {
      isUnlocked = true
      print("other type")
    }
  }
  
  class func isFaceIDPossible() -> Bool {
    getBiometricType() == .face
  }
  
  class func isTouchIDPossible() -> Bool {
    getBiometricType() == .touch
  }
  
  class func getBiometricType() -> BiometricType{
    
    let authenticationContext = LAContext()
    let _ = authenticationContext.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)
    switch (authenticationContext.biometryType){
    case .faceID:
      return .face
    case .touchID:
      return .touch
    default:
      return .none
    }
  }
  
}
