//
//  ContentView.swift
//  Shared
//
//  Created by Michael Helmbrecht on 09.10.20.
//

import SwiftUI

struct MainAppView: View {
  @State private var activeTab = 0
  @EnvironmentObject var store: AppStore
  var body: some View {
    TabView(selection: $activeTab){
      HomeContainerView(store: store)
        .tag(0)
        .tabItem {
          VStack {
            Image(systemName: Icon.tracken.rawValue)
            Text("Tracken")
          }
        }
      DiaryContainerView(store: store).tag(1)
        .tabItem {
        VStack {
          Image(systemName: Icon.diary.rawValue)
          Text("Tagebuch")
        }
      }
      DirectoryContainerView().tag(2)
        .tabItem {
        VStack {
          Image(systemName: Icon.directory.rawValue)
          Text("Verzeichnis")
        }
      }
      Text("Statistik").tag(3)
        .tabItem {
        VStack {
          Image(systemName: Icon.statistics.rawValue)
          Text("Statistik")
        }
      }
    }
  }
}

struct ContentView_Previews: PreviewProvider {
  static var previews: some View {
    MainAppView()
  }
}
