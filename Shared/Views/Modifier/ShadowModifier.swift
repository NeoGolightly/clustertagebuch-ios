//
//  ShadowModifier.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 20.10.20.
//

import SwiftUI


struct ButtonShadow: ViewModifier {
    func body(content: Content) -> some View {
        content
          .shadow(color: Color.black.opacity(0.07), radius: 80, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 100)
          .shadow(color: Color.black.opacity(0.04), radius: 17, x: 0.0, y: 22)
          .shadow(color: Color.black.opacity(0.16), radius: 12, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 6)
    }
}

struct ButtonShadow2: ViewModifier {
    func body(content: Content) -> some View {
        content
          .shadow(color: Color.black.opacity(0.10), radius: 80, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 100)
          .shadow(color: Color.black.opacity(0.20), radius: 30, x: 0.0, y: 22)
          .shadow(color: Color.black.opacity(0.16), radius: 12, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 6)
    }
}

struct CardShadow: ViewModifier {
    func body(content: Content) -> some View {
        content
          .shadow(color: Color.black.opacity(0.3), radius: 16, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 4)
          .shadow(color: Color.black.opacity(0.04), radius: 10, x: 0.0, y: 12)
//          .shadow(color: Color.black.opacity(0.16), radius: 12, x: /*@START_MENU_TOKEN@*/0.0/*@END_MENU_TOKEN@*/, y: 6)
    }
}

extension View {
    func buttonShadow() -> some View {
        self.modifier(ButtonShadow())
    }
  
  func buttonShadow2() -> some View {
      self.modifier(ButtonShadow())
  }
  
  func cardShadow() -> some View {
      self.modifier(CardShadow())
  }
}
