//
//  StandardButton.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 17.10.20.
//

import SwiftUI

struct StandardButton: View {
  let title: String
  let action: () -> Void
  var body: some View {
    Button(action: action,
           label: {
            Text(title)
              .padding()
              .padding([.leading, .trailing], 16)
              .foregroundColor(.white)
              .background(Color(red: 186/255, green: 86/255, blue: 137/255).cornerRadius(8))
           })
  }
}
