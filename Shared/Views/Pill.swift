//
//  Pill.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 18.10.20.
//

import SwiftUI

struct Pill: View {
  var title: String = ""
  var color: Color = Color(hex: "E98E5B")
  var selected = false
  
  init(title: String) {
    self.title = title
  }
  
  init(title: String, color: Color) {
    self.title = title
    self.color = color
  }
  
  init(title: String, color: Color, selected: Bool) {
    self.title = title
    self.color = color
    self.selected = selected
  }
  
  init(title: String, hexColor: String) {
    self.title = title
    self.color = Color(hex: hexColor)
  }
  var body: some View {
    ZStack {
      Capsule().stroke(color, lineWidth: selected ? 1 : 1)
        .background(Capsule().fill(selected ? color.opacity(0.2) : .clear))
        .frame(minHeight:20, maxHeight: 70)
      Text(title)
        .font(Font.caption.leading(.tight).bold()).padding([.leading, .trailing], 8)
        .lineLimit(2)
//        .foregroundColor(selected ? .white :.primary)
      
    }.frame(minHeight: 40)
  }
}

struct PillTiny: View {
  var title: String = ""
  var color: Color = Color(hex: "E98E5B")
  
  init(title: String) {
    self.title = title
  }
  
  init(title: String, color: Color) {
    self.title = title
    self.color = color
  }
  
  init(title: String, hexColor: String) {
    self.title = title
    self.color = Color(hex: hexColor)
  }
  
  var body: some View {
    Text(title)
      .font(Font.caption.leading(.tight).bold())

      .padding([.leading, .trailing], 16)
      .lineLimit(2)
      .background(Capsule().stroke(color).foregroundColor(.clear))
//    ZStack {
//      Capsule().stroke(color).foregroundColor(.clear).frame(minWidth: 50, minHeight:30, maxHeight: 40)
//      Text(title).font(Font.caption.leading(.tight).bold()).padding([.leading, .trailing], 12).lineLimit(2)
//    }
  }
}

struct Pill_Previews: PreviewProvider {
    static var previews: some View {
      Group {
        Pill(title: "Michael Helmbrecht")
          .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
        PillTiny(title: "Michael Helmbrecht")
          .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/))
      }
    }
}
