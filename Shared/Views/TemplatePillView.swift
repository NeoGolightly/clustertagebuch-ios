//
//  TemplatePillView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 21.10.20.
//

import SwiftUI

struct TemplatePillView: View {
  @Binding var template: Template
  var body: some View {
    Button(action: {}, label: {
      VStack(alignment: .leading) {
        HStack(alignment: .firstTextBaseline) {
          Text(template.name).font(.title2).bold()
          Spacer()
          if template.sendNotifications {
            Image(systemName: "repeat").font(.callout)
            Image(systemName: "bell.fill").font(.callout)
          }
        }
        Text(template.place?.name ?? "").font(.caption).bold().foregroundColor(.primary)
        if template.isRepeating {
          VStack(alignment: .leading) {
            HStack(alignment: .firstTextBaseline, spacing: 8) {
              
              VStack(alignment: .leading) {
                HStack(spacing: 0) {
                  Text(template.repeatingType.rawValue).font(.caption).bold().foregroundColor(Color.primary)
                  Text(" (\(template.repeatingDay?.rawValue ?? ""))").font(.caption).bold().foregroundColor(Color.primary)
                }
                if let startTime = template.startTime,
                   let endTime = template.endTime {
                  HStack(spacing: 0) {
                    Text(getMinuteAndHour(date: startTime)).foregroundColor(.primary).font(.caption)
                    Text(" – ").foregroundColor(.primary).font(.caption)
                    Text(getMinuteAndHour(date: endTime)).foregroundColor(.primary).font(.caption)
                  }
                }
              }
              
            }.padding(.vertical, 2)
            
          }
        }
        Spacer()
      }.padding()
      .background(RoundedRectangle(cornerRadius: 16)
                    .stroke(Color.accentColor, lineWidth: 1.0)
                    .foregroundColor(Color.white)
                    .cardShadow()
      )
    })
  }
  
  private func getMinuteAndHour(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    return dateFormatter.string(from: date)
  }
}

struct TinyTemplatePillView: View {
  @Binding var template: Template
  var body: some View {
    Button(action: {}, label: {
      VStack(alignment: .leading, spacing: 0) {
        HStack(alignment: .firstTextBaseline, spacing: 4) {
          Text(template.name).font(Font.caption.bold()).foregroundColor(.primary)
          
          if template.sendNotifications {
            Spacer(minLength: 12)
            Image(systemName: "arrow.triangle.2.circlepath").font(.caption)
            Image(systemName: "bell.fill").font(.caption)
          }
        }

      }
      .padding()
      .background(RoundedRectangle(cornerRadius: .infinity)
                    .stroke(Color.accentColor, lineWidth: 1.0)
                    .foregroundColor(Color.white)
                    .cardShadow()
      )
    })
  }
  
  private func getMinuteAndHour(date: Date) -> String {
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "HH:mm"
    return dateFormatter.string(from: date)
  }
}

struct TemplatePillView_Previews: PreviewProvider {
  static var previews: some View {
    Group {
      ScrollView {
        TemplatePillView(template: .constant(Template.mocks().first!))
          .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/300.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/)).padding()
      }
      ScrollView {
        TinyTemplatePillView(template: .constant(Template.mocks().first!))
          .previewLayout(.fixed(width: /*@START_MENU_TOKEN@*/300.0/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100.0/*@END_MENU_TOKEN@*/)).padding()
      }
    }
  }
}
