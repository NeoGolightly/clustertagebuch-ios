//
//  MediumWidgetView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 04.11.20.
//

import SwiftUI
import WidgetKit

struct MediumWidgetView: View {
  var entry: StaticProvider.Entry
  var body: some View {
    VStack(alignment: .leading, spacing: 0) {
      HStack(alignment: .lastTextBaseline) {
        VStack(alignment: .leading) {
          Text("Kontakte")
            .font(Font.body)
            .bold()
            .foregroundColor(.primary)
          Text("heute")
            .font(.caption)
            .foregroundColor(.gray)
        }
        Spacer()
        HStack() {
          Text(numberFormat(number: entry.peopleCount))
            .font(.system(.title2, design: .monospaced))
            .foregroundColor(.color1).bold()
          Text(entry.peopleCount == 1 ? "Person" : "Personen")
            .font(.system(.caption, design: .monospaced))
            .foregroundColor(.primary)
        }
        HStack(alignment: .lastTextBaseline) {
          Text(numberFormat(number: entry.placesCount))
            .font(.system(.title2, design: .monospaced))
            .foregroundColor(.color2).bold()
          Text(entry.placesCount == 1 ? "Ort" : "Orte")
            .font(.system(.caption, design: .monospaced))
            .foregroundColor(.primary)
        }
      }
     
      Divider().padding(.bottom, 4)
      VStack {
        Spacer()
        HStack {
          Spacer()
          Button(action: {}, label: {
            Image(systemName: "plus.circle.fill")
              .font(.largeTitle)
              .foregroundColor(.color3)
          })
          
        }
      }
    }
    .padding()
  }
}

struct MediumWidgetView_Previews: PreviewProvider {
  static var previews: some View {
    MediumWidgetView(entry: StaticEntry.placeholder())
      .previewContext(WidgetPreviewContext(family: .systemMedium))
  }
}
