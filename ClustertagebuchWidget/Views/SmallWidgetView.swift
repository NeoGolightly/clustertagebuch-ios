//
//  SmallWidgetView.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 04.11.20.
//

import SwiftUI
import WidgetKit
import SwiftDate

struct SmallWidgetView: View {
  var entry: StaticProvider.Entry
  
  private func dateLabel(date: Date) -> String {
    if date.isToday {
      return "Heute"
    }
    if date.isYesterday {
      return "Gestern"
    }
    
    return date.toFormat("dd.MM.yy")
  }
  
  var body: some View {
    VStack(alignment: .leading, spacing: 0) {
      Spacer()
      Text("Kontakte")
        .font(Font.body)
        .bold()
        .foregroundColor(.primary)
      Text("heute")
        .font(.caption)
        .foregroundColor(.gray)
      Divider().padding(.bottom, 4)
      HStack(alignment: .firstTextBaseline) {
        Text(numberFormat(number: entry.peopleCount))
          .font(.system(.title, design: .monospaced))
          .foregroundColor(.color1).bold()
        Text(entry.peopleCount == 1 ? "Person" : "Personen")
          .font(.system(.caption, design: .monospaced))
          .foregroundColor(.primary)
      }
      HStack(alignment: .lastTextBaseline) {
        Text(numberFormat(number: entry.placesCount))
          .font(.system(.title, design: .monospaced))
          .foregroundColor(.color2).bold()
        Text(entry.placesCount == 1 ? "Ort" : "Orte")
          .font(.system(.caption, design: .monospaced))
          .foregroundColor(.primary)
      }
      Spacer()
    }
    .padding()
  }
}

struct SmallWidgetView_Previews: PreviewProvider {
    static var previews: some View {
      SmallWidgetView(entry: StaticEntry.placeholder())
          .previewContext(WidgetPreviewContext(family: .systemSmall))
    }
}
