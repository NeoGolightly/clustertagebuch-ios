//
//  ClustertagebuchWidget.swift
//  ClustertagebuchWidget
//
//  Created by Michael Helmbrecht on 22.10.20.
//

import WidgetKit
import SwiftUI
import Intents

func numberFormat(number: Int) -> String {
  return number < 10 ? "0\(number)" : "\(number)"
}

struct ClustertagebuchWidgetEntryView : View {
  @Environment(\.widgetFamily) var widgetFamily
  @EnvironmentObject var store: AppStore
  var entry: StaticProvider.Entry
  var body: some View {
    switch widgetFamily {
    case .systemSmall:
      SmallWidgetView(entry: entry)
        .onAppear(perform: {
          store.send(.load)
      })
    case .systemMedium:
      MediumWidgetView(entry: entry)
        .onAppear(perform: {
          store.send(.load)
      })
    @unknown default:
      EmptyView()
    }
  }
  
}

@main
struct ClustertagebuchWidget: Widget {
  let kind: String = "ClustertagebuchWidgetStatic"
  @StateObject var store = AppStore(initialState: AppState(),
                                    reducer: appReducer,
                                    environment: AppEnvironment())
  
  var body: some WidgetConfiguration {
    StaticConfiguration(kind: kind, provider: StaticProvider(store: store)) { (entry) in
      ClustertagebuchWidgetEntryView(entry: entry)
        .environmentObject(store)
    }
//    IntentConfiguration(kind: kind,
//                        intent: ConfigurationIntent.self,
//                        provider: Provider(store: store)) { entry in
//      ClustertagebuchWidgetEntryView(entry: entry)
//    }
    .configurationDisplayName("Clustertagebuch")
    .description("This is an example widget.")
//    .supportedFamilies([.systemMedium])
  }
}

//struct ClustertagebuchWidget_Previews: PreviewProvider {
//  static var previews: some View {
//    Group {
//      ClustertagebuchWidgetEntryView(entry: StaticEntry(date: Date(), peopleCount: 14, placesCount: 4))
//        .previewContext(WidgetPreviewContext(family: .systemSmall))
//      ClustertagebuchWidgetEntryView(entry: StaticEntry(date: Date(), peopleCount: 14, placesCount: 4))
//        .previewContext(WidgetPreviewContext(family: .systemMedium))
//      ClustertagebuchWidgetEntryView(entry: StaticEntry(date: Date(), peopleCount: 14, placesCount: 4))
//        .previewContext(WidgetPreviewContext(family: .systemLarge))
//    }
//  }
//}
