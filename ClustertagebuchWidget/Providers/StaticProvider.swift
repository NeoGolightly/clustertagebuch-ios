//
//  StaticProvider.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 04.11.20.
//

import WidgetKit

struct StaticEntry: TimelineEntry {
  let date: Date
  let day: Date
  let peopleCount: Int
  let placesCount: Int
}

extension StaticEntry {
  static func placeholder() -> StaticEntry {
    StaticEntry(date: Date(),
                day: Date(),
                peopleCount: 12,
                placesCount: 4)
  }
}

struct StaticProvider: TimelineProvider{
  var store: AppStore
  func placeholder(in context: Context) -> StaticEntry {
    StaticEntry.placeholder()
  }
  func getSnapshot(in context: Context, completion: @escaping (StaticEntry) -> Void) {
    var entry: StaticEntry
    if context.isPreview {
      entry = StaticEntry(date: Date(), day: Date(), peopleCount: 2, placesCount: 10)
    } else {
      store.send(.load)
      let lastDay = getLastDay()
      entry = StaticEntry(date: Date(),
                          day: lastDay?.day ?? Date(),
                          peopleCount: lastDay?.contacts.peopleCount() ?? 0,
                          placesCount: lastDay?.contacts.placesCount() ?? 0)
    }
    completion(entry)
  }
  func getTimeline(in context: Context, completion: @escaping (Timeline<StaticEntry>) -> Void) {
    if context.isPreview {
      let entry = StaticEntry(date: Date(),
                              day: Date(),
                              peopleCount:42,
                              placesCount: 68)
      completion(Timeline(entries: [entry], policy: .never))
      return
    }
    store.send(.load)
    let lastDay = getLastDay()
    let entry = StaticEntry(date: Date(),
                            day: lastDay?.day ?? Date(),
                            peopleCount: lastDay?.contacts.peopleCount() ?? 0,
                            placesCount: lastDay?.contacts.placesCount() ?? 0)
    let timeLine = Timeline(entries: [entry], policy: TimelineReloadPolicy.never)
    completion(timeLine)
  }
  
  private func getLastDay() -> (day:Date , contacts: [Contact])? {
    store.state
      .allContacts
      .clusteredContacts(contacts: store.state.contacts)
      .sorted(by: {$0.key < $1.key})
      .last
      .map{($0.key, $0.value)}
      
  }
}
