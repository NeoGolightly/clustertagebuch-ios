//
//  IntentProvider.swift
//  Clustertagebuch (iOS)
//
//  Created by Michael Helmbrecht on 04.11.20.
//

import WidgetKit

struct Provider: IntentTimelineProvider {
  
  var store: AppStore
  
  func placeholder(in context: Context) -> SimpleEntry {
    SimpleEntry(date: Date(), configuration: ConfigurationIntent(), peopleCount: 2, placesCount: 42)
  }
  
  func getSnapshot(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (SimpleEntry) -> ()) {
    
    
    store.send(.load)
    let entry = SimpleEntry(date: Date(),
                            configuration: configuration,
                            peopleCount: 10,
                            placesCount: 10)
    completion(entry)
  }
  
  func getTimeline(for configuration: ConfigurationIntent, in context: Context, completion: @escaping (Timeline<Entry>) -> ()) {
    store.send(.load)
    var entries: [SimpleEntry] = []
    
    // Generate a timeline consisting of five entries an hour apart, starting from the current date.
    let currentDate = Date()
    for hourOffset in 0 ..< 5 {
      let entryDate = Calendar.current.date(byAdding: .hour, value: hourOffset, to: currentDate)!
      let entry = SimpleEntry(date: entryDate,
                              configuration: configuration,
                              peopleCount: store.state.people.count,
                              placesCount: store.state.places.count)
      entries.append(entry)
    }
    
    let entry = SimpleEntry(date: currentDate,
                            configuration: configuration,
                            peopleCount: store.state.people.count,
                            placesCount: store.state.places.count)
    
    let timeline = Timeline(entries: [entry], policy: .never)
    completion(timeline)
  }
}

struct SimpleEntry: TimelineEntry {
  let date: Date
  let configuration: ConfigurationIntent
  let peopleCount: Int
  let placesCount: Int
}
